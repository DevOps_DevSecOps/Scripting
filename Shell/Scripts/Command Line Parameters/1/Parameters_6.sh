#!/bin/bash

# Program name: "total_command.sh"
# Linux shell script program to demonstrate the "$#" variable.
echo "Total command line arguments are: $#"
