#!/bin/bash

string1="Hello, "
string2="World!"
concatenated=$string1$string2

original="Hello, World! Hello!"
search="Hello"
replace="Hi"
replaced=${original//$search/$replace}

echo "Concatenated string: $concatenated"
echo "Original string: $original"
echo "Replaced string: $replaced"

substring=${replaced:0:5}
echo "Extracted substring: $substring"


# MultipleLine Comments
: '

$ ./String_1.sh
Concatenated string: Hello, World!
Original string: Hello, World! Hello!
Replaced string: Hi, World! Hi!
Extracted substring: Hi, W

'
