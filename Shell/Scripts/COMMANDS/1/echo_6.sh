#!/bin/bash

# Running commands in a subshell and background process
(
    echo "Subshell working directory: $(pwd)"
    sleep 3 &
    echo "Subshell background process started."
    wait
    echo "Subshell background process completed."
)
echo "Main shell continues."


# MultipleLine Comments
: '

$ ./echo_6.sh
Subshell working directory: /home/user
Subshell background process started.
[1]+ Done sleep 3
Subshell background process completed.
Main shell continues.

'
