#!/bin/bash
# Running commands in a subshell
echo "Current working directory: $(pwd)"
echo "Number of files in /tmp: $(ls /tmp | wc -l)"


# MultipleLine Comments
: '

$ bash echo_4.sh
Current working directory: /home/user
Number of files in /tmp: 10

'
