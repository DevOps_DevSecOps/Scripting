#!/bin/bash

string="Hello, World! Hello!"
search="Hello"
replace="Hi"
result=${string//$search/$replace} # Replace all occurrences

echo "Original string: $string"
echo "Result after replacement: $result"


# MultipleLine Comments
: '

$ bash Variables-1.sh
Original string: Hello, World! Hello!
Result after replacement: Hi, World! Hi!

'
