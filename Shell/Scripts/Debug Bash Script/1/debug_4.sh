#!/bin/bash

set -x
set -e

name="John"
age=25

echo "Debug: Starting script..."
echo "Debug: Name is $name"
echo "Debug: Age is $age"

result=$((age * 2))
echo "Debug: Result is $result"

# Introducing an intentional error to demonstrate set -e
ls /nonexistent_directory

echo "Debug: Script completed."

# set -x command shows each executed command with a + sign
# set -e command causes the script to exit immediately after the ls command fails
# to turn off debugging mode, use set +x


# MultipleLine Comments
: '

$ ./debug_4.sh
+ name=John
+ age=25
+ echo 'Debug: Starting script...'
Debug: Starting script...
+ echo 'Debug: Name is John'
Debug: Name is John
+ echo 'Debug: Age is 25'
Debug: Age is 25
+ result=50
+ echo 'Debug: Result is 50'
Debug: Result is 50
+ ls /nonexistent_directory
ls: cannot access '/nonexistent_directory': No such file or directory
+ echo 'Debug: Script completed.'
Debug: Script completed.

'
