#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
set -e

# Check if mongosh is installed
if ! command -v mongosh &> /dev/null; then
    echo "mongosh could not be found. Please install it before running this script."
    exit 1
fi

# Define MongoDB connection details (improve security with environment variables)
MONGODB_USERNAME="${MONGODB_USERNAME:-}"
MONGODB_PASSWORD="${MONGODB_PASSWORD:-}"

if [[ -z "$MONGODB_USERNAME" || -z "$MONGODB_PASSWORD" ]]; then
    echo "MongoDB username or password is not set. Please set the MONGODB_USERNAME and MONGODB_PASSWORD environment variables."
    exit 1
fi

# Trigger MongoDB log rotation
mongo_command="db.adminCommand({ logRotate: 1 })"
if ! mongosh_output=$(mongosh --eval "$mongo_command" -u "$MONGODB_USERNAME" -p "$MONGODB_PASSWORD" 2>&1); then
    echo "Failed to rotate MongoDB logs. Error: $mongosh_output"
    exit 1
fi

echo "Log rotation command executed. Waiting for the rotation to complete..."
# Wait for log rotation to complete (adjust based on expected rotation time)
sleep 120

# Define search criteria for old log files
log_dir="/var/log/mongodb/"
log_prefix="mongod.log.2024"
max_file_age=5 # Files older than 5 days will be removed

# Find and delete old log files
if find "$log_dir" -name "${log_prefix}*" -mtime +"$max_file_age" -exec rm -rf {} \;; then
    echo "MongoDB log rotation and cleanup completed successfully."
else
    echo "Error occurred during the cleanup of old log files."
    exit 1
fi