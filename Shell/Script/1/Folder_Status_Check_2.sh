#!/bin/bash

echo "Starting script..."

ls /nonexistent-directory
if [ $? -eq 0 ]; then
    echo "Directory exists."
else
    echo "Directory does not exist."
fi

echo "Script finished."


# MultipleLine Comments
: '

$ bash Folder_Status_Check_2.sh
Starting script...
ls: cannot access '/nonexistent-directory': No such file or directory
Directory does not exist.
Script finished.

'
