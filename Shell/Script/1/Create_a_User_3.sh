NEW_USER=newuser

sudo adduser --disabled-password "$NEW_USER"
sudo -i -u "$NEW_USER"

# now you are under the new user's shell
cd
mkdir -p .ssh
chmod 0700 .ssh
echo "<SSH_PUB_KEY>" > .ssh/authorized_keys
chmod 600 .ssh/authorized_keys
