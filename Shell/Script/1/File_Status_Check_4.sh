#!/bin/bash

file="nonexistent-file.txt"

if [ ! -f "$file" ]; then
    echo "Error: File '$file' does not exist."
    exit 1
fi

echo "File '$file' exists."


# MultipleLine Comments
: '

$ bash File_Status_Check_4.sh
Error: File 'nonexistent-file.txt' does not exist.

'
