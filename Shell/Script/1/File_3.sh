#!/bin/bash

file_name="file_1.txt"
file_path="."                                   # file path for "file_1.txt"
files_list=$(find "$file_path" -name "$file_name")

for file in $file_name
do
    if [ -e $file ]
    then
        sed 's/Apples/Oranges/g' $file
    fi
done

# it show the output only after run the script.
