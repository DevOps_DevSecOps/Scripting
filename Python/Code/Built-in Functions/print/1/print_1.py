# Strings in python are surrounded by either single quotation ('') marks OR double quotation ("") marks.

print('Hello')

print("Welcome")
